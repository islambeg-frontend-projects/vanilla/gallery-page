module.exports = {
  extends: ["stylelint-config-standard-scss", "stylelint-config-prettier-scss"],
  ignoreFiles: [
    "**/*/*.js",
    "**/*/*.html",
    "src/**/*.css",
    "src/**/*.map",
    "src/assets/**/*",
  ],
  rules: {
    "font-family-name-quotes": "always-unless-keyword",
  },
};
