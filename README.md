<h1 align="center">Gallery Page</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/vanilla/gallery-page">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/vanilla/gallery-page">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/gcbWLxG6wdennelX7b8I">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

## Overview

This is a simple page with a gallery.

### Built With

- HTML
- CSS
- [Sass](https://sass-lang.com/)

## Acknowledgements

- [A modern CSS Reset](https://piccalil.li/blog/a-modern-css-reset/)
- [Favicon by Remix Design](https://remixicon.com/)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
